// disable scroll keyboard

$.ready(() => {
	cordova.plugins.Keyboard.disableScroll(true);
})

// now, js!

const $ = new DisplayJS(window)
const App = new MainApp();


let state = 0;
$.render();
game()

$.on(".play", "click", e => {
    state = 1;
    $.render()
})
$.all(".back", el => {
    $.on(el, "click", e => {
        state = 0;
        $.render()
    })
})
$.on(".settings", "click", e => {
    state = 2;
    settings()
    $.render()
})

function settings() {
    function init() {
        $.single(".digit").value = App.set.nLength

    }
    init()
}

function game() {
    function init() {
        App.init()
        op = App.getOp()
        $.html(".operation", op[0])
    }
    init()
}

function shooting_star(el, prop, t) {
	setInterval(function() {
	    var topPos = Math.floor(Math.random() * 80) + 1,
	        topPos = topPos + '%',
	        leftPos = Math.floor(Math.random() * 40) + 1,
	        leftPos = leftPos + '%',
	        trans = Math.floor(Math.random() * 300) + 1,
	        trans = trans + 'deg';
	    $.css(el, {
	        'top': topPos,
	        prop: leftPos,
	        'transform': 'rotate(' + trans + ')'
	    });
	}, t);
}
shooting_star($.toNodeList($.s(".shooting-stars")[0]), 'left', 2000)
shooting_star($.toNodeList($.s(".shooting-stars")[1]), 'right', 2000)



// target
$.target(() => {
	App.set.nLength = parseInt('digits' in window ? digits : 1)
	App.set.cType = 'type' in window ? type : "mul"
	if (App.isClose(op[1], parseFloat('result' in window ? result : "0")) == true) {
		$.text(".plus", "+" + parseFloat(result))
		setTimeout(() => {
			$.text(".plus", "")
		}, 1000)
		$.text(".score", parseInt($.single(".score").innerHTML) + parseFloat(result))
		$.valEmpty(".out")
		game()
	}
})
